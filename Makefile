# I'm old school and my reflex is to type make {clean, all, test, check, build, ...}
#
CARGO=cargo
DBGTGT=
TAGS=tags
EXAMPLESRC=$(wildcard examples/*.rs)
EXAMPLES=$(EXAMPLESRC:examples/%.rs=%)

SRC=src/*.rs /home/sam/rust/rtlsdr_ffi/target/debug/build/rtlsdr_ffi-*/out/bindings.rs

.PHONY: all build check clean clippy fmt test run examples help

all: ${TAGS} ${DBGTGT}

build check clean clippy fmt:
	clear
	${CARGO} $@

examples: ${EXAMPLESRC}
	clear
	echo "Building examples" $(EXAMPLESRC)
	${CARGO} build --examples
	${CARGO} run --example ${EXAMPLES}

test:
	clear
	${CARGO} test -- --nocapture --color always

run: ${DBGTGT}
	clear
	${CARGO} $@ -- ${RUNFLAGS}

help: ${DBGTGT}
	${CARGO} run -- --help

${TAGS}: ${SRC}
	ctags ${SRC}
	-git commit -m "Update tags" tags

${DBGTGT}: ${SRC}
	${CARGO} build
